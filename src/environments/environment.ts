// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCqo59lAEihTWxXU5i6bjW4XK_bvrgb6Yk',
    databaseURL: 'https://imdb-imageupload-default-rtdb.firebaseio.com/',
    authDomain: 'imdb-imageupload.firebaseapp.com',
    projectId: 'imdb-imageupload',
    storageBucket: 'imdb-imageupload.appspot.com',
    messagingSenderId: '604795304099',
    appId: '1:604795304099:web:8e7f44e184414e095ce716',
    measurementId: 'G-7F578VM040',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
