import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Actors } from '../models/actors.models';
import { Movies } from '../models/movies.model';
import { Producers } from '../models/producers.model';

@Injectable({
  providedIn: 'root',
})
export class MoviesService {
  private serverURL = 'http://localhost:3000/movies';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(private http: HttpClient) {}

  getMovies(): Observable<any> {
    return this.http.get<any>(this.serverURL + '/all');
  }

  getActors(): Observable<any> {
    return this.http.get<any>(this.serverURL + '/actors');
  }

  getProducers(): Observable<any> {
    return this.http.get<any>(this.serverURL + '/producers');
  }

  getMovie(movieid: any) {
    return this.http.get<any>(this.serverURL + `/${movieid}`);
  }

  addActor(actor: Actors) {
    return this.http
      .post<Actors>(this.serverURL + '/addactor', actor, this.httpOptions)
      .pipe(retry(1), catchError(this.handleError));
  }

  addProducer(producer: Producers) {
    return this.http
      .post<Producers>(
        this.serverURL + '/addproducer',
        producer,
        this.httpOptions
      )
      .pipe(retry(1), catchError(this.handleError));
  }

  updateMovie(movie: any) {
    return this.http.post<any>(
      this.serverURL + '/updatemovie',
      movie,
      this.httpOptions
    );
  }

  addMovie(movie: any) {
    return this.http.post<any>(
      this.serverURL + '/addmovie',
      movie,
      this.httpOptions
    );
  }

  deleteMovie(movieid: any) {
    return this.http.delete<any>(this.serverURL + '/delete/' + movieid);
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code. // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    } // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }
}
