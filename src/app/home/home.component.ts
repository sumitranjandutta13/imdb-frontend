import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Movies } from '../models/movies.model';
import { MoviesService } from '../shared/movies.service';
import {
  NgbModal,
  ModalDismissReasons,
  NgbModalOptions,
} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  public movies: Movies[] = [];
  public responseData: any;

  public deleteMovie = '';

  modalOptions: NgbModalOptions;
  constructor(
    private movieService: MoviesService,
    private router: Router,
    private modalService: NgbModal
  ) {
    this.modalOptions = {
      backdrop: 'static',
      backdropClass: 'customBackdrop',
    };
  }

  ngOnInit(): void {
    this.movieService.getMovies().subscribe((response: any) => {
      this.responseData = {};
      response.data.forEach((element: any) => {
        if (this.responseData[element.movieid] === undefined) {
          this.responseData[element.movieid] = {};
          this.responseData[element.movieid]['movieid'] = element.movieid;
          this.responseData[element.movieid]['name'] = element.movieName;
          this.responseData[element.movieid]['year'] = element.year;
          this.responseData[element.movieid]['plot'] = element.plot;
          this.responseData[element.movieid]['poster'] = element.poster;
          this.responseData[element.movieid]['actors'] = [element.actorName];
          this.responseData[element.movieid]['producer'] = element.prodName;
        } else {
          this.responseData[element.movieid]['actors'].push(element.actorName);
        }
      });
      for (let key in this.responseData) {
        this.movies.push(this.responseData[key]);
      }
      console.log(this.movies);
    });
  }

  editMovie(movieid: any) {
    this.router.navigate(['/updatemovie', movieid]);
  }

  open(content: any, movie: any) {
    this.deleteMovie = movie.name;
    this.modalService.open(content, this.modalOptions).result.then(
      (result) => {
        this.movieService.deleteMovie(movie.movieid).subscribe((res) => {
          if (res.message === 'success') {
            this.movieService.getMovies().subscribe((response: any) => {
              this.responseData = {};
              response.data.forEach((element: any) => {
                if (this.responseData[element.movieid] === undefined) {
                  this.responseData[element.movieid] = {};
                  this.responseData[element.movieid]['movieid'] =
                    element.movieid;
                  this.responseData[element.movieid]['name'] =
                    element.movieName;
                  this.responseData[element.movieid]['year'] = element.year;
                  this.responseData[element.movieid]['plot'] = element.plot;
                  this.responseData[element.movieid]['poster'] = element.poster;
                  this.responseData[element.movieid]['actors'] = [
                    element.actorName,
                  ];
                  this.responseData[element.movieid]['producer'] =
                    element.prodName;
                } else {
                  this.responseData[element.movieid]['actors'].push(
                    element.actorName
                  );
                }
              });
              this.movies = [];
              for (let key in this.responseData) {
                this.movies.push(this.responseData[key]);
              }
              console.log(this.movies);
            });
          }
        });
      },
      (reason) => {
        console.log(reason);
      }
    );
  }
}
