export class Movies {
  public movieid: any;
  public name: any;
  public year: any;
  public plot: any;
  public poster: any;
  public actors: any;
  public producer: any;
}
