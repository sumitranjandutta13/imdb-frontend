import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Actors } from '../models/actors.models';
import { Movies } from '../models/movies.model';
import { Producers } from '../models/producers.model';
import { MoviesService } from '../shared/movies.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { FileuploadService } from '../shared/fileupload.service';
import { FileUpload } from '../models/fileUpload.models';

@Component({
  selector: 'app-update-movie',
  templateUrl: './update-movie.component.html',
  styleUrls: ['./update-movie.component.css'],
})
export class UpdateMovieComponent implements OnInit {
  public file?: File;
  public actors: Actors[] = [];
  public producers: Producers[] = [];
  public movie: any;
  public movieid: any;
  public selectedItems: any;

  public currentFileUpload?: FileUpload;

  percentage = 0;

  public changePoster: Boolean = false;

  dropDownList: any = [];
  dropdownSettings: IDropdownSettings = {};

  constructor(
    private http: HttpClient,
    private movieService: MoviesService,
    private route: ActivatedRoute,
    private uploadService: FileuploadService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.movieService.getActors().subscribe((response: any) => {
      this.actors = response.data;
      this.dropDownList = this.actors.map((actor: any) => {
        return { item_id: actor.actorid, item_text: actor.name };
      });
    });
    this.movieService.getProducers().subscribe((response: any) => {
      this.producers = response.data;
    });
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.movie = {};
      this.movieid = params.get('movieid');
      this.movieService.getMovie(this.movieid).subscribe((response: any) => {
        this.movie.name = response.data[0].movieName;
        this.movie.year = response.data[0].year;
        this.movie.plot = response.data[0].plot;
        this.movie.poster = response.data[0].poster;
        this.movie.producerid = response.data[0].producerid;
        this.selectedItems = response.data.map((element: any) => {
          return { item_id: element.actorid, item_text: element.actorName };
        });
        console.log(this.selectedItems);
      });
    });
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 6,
      allowSearchFilter: true,
    };
  }

  check(id: any) {
    return id === this.movie.producerid;
  }

  onFileChange(event: any) {
    this.changePoster = true;
    this.file = event.target.files[0];
  }

  async updateMovie(
    name: any,
    year: any,
    plot: any,
    actors: any,
    producer: any,
    poster: any
  ) {
    if (this.changePoster) {
      this.movieService.deleteMovie(this.movieid).subscribe((res) => {
        if (res.message === 'success') {
          actors = actors.map((element: any) => {
            return element.id;
          });
          const movie = {
            movieid: this.movieid,
            name: name,
            year: year,
            plot: plot,
            actors: actors,
            producer: parseInt(producer),
          };
          if (this.file) {
            this.currentFileUpload = new FileUpload(this.file);
            this.uploadService
              .pushFileToStorage(this.currentFileUpload, movie)
              .subscribe(
                (percentage) => {
                  this.percentage = Math.round(percentage ? percentage : 0);
                },
                (error) => {
                  console.log(error);
                }
              );
          }
        }
      });
    } else {
      this.movieService.deleteMovie(this.movieid).subscribe((res) => {
        if (res.message === 'success') {
          actors = actors.map((element: any) => {
            return element.id;
          });
          const movie = {
            movieid: this.movieid,
            name: name,
            year: year,
            plot: plot,
            actors: actors,
            producer: parseInt(producer),
            poster: this.movie.poster,
          };
          this.movieService.updateMovie(movie).subscribe((res) => {
            if (res.message === 'success') {
              this.router.navigate(['/']);
            }
          });
        }
      });
    }
  }
}
